import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, View } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import * as Toast from "nativescript-toast";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";
import { stringify } from "@angular/compiler/src/util";
import * as SocialShare from "nativescript-social-share";
import { ImageSource } from "image-source";

const sqlite = require("nativescript-sqlite");
const socialShare = require("nativescript-social-share");
@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    providers: [NoticiasService]
})
export class SearchComponent implements OnInit {
    resultados: Array<string> = [];
    @ViewChild("layout") layout : ElementRef;
    
    constructor(private noticias: NoticiasService, private store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }
    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        setTimeout(() => {
        this.resultados.push("xxxxxxx");
        pullRefresh.refreshing = false;
        }, 2000);
       } 

    eliminarElemento(): void{
        dialogs.alert({
            title: "Se eliminó el elemento",
            message: "El elemento seleccionado se eliminó correctamente",
            okButtonText: "Ok"
        }).then(() => {
            console.log("Dialog closed!");
        });
    }
    mostrarDialogAction(): void{
        dialogs.action({
            message: "Selecciona la categoria que quieras editar",
            cancelButtonText: "Cancelar",
            actions: ["Elemento1", "Elemento2"]
        }).then(result => {
            console.log("Resultado del diálogo: " + result);
            if(result == "Elemento1"){
                var toast = Toast.makeText("Se editó el Elemento1");
                toast.show();
            }else if(result == "Elemento2"){
                var toast = Toast.makeText("Se editó el Elemento2");
                toast.show();
            }
        });
    }
    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
        .subscribe((data) => {
            const f = data;
            if(f != null){
                Toast.show({text: "Sugerimos leer" + f.titulo, duration: Toast.duration.SHORT});
            }
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
    onItemTap(args): void {
       this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }
    onLongPress(s): void {
        console.log(s);
        socialShare.shareText(s, "Asunto: compartido desde el curso!");
    }
    compartirImagen(s): void{
        const imagenCompartida =  ImageSource.fromFile("~/App_Resources/src/main/res/drawable-hdpi/menu.png");
        SocialShare.shareImage(imagenCompartida);
    }
    buscarAhora(s: string){
      console.dir("Buscar Ahora" +s);
      this.noticias.buscar(s).then((r: any)=> {
          console.log("resultado buscar ahora:" + JSON.stringify(r));
          this.resultados = r;
      }, (e) => {
          console.log("error buscar ahora"+ e);
          Toast.show(
              {text: "Erorr en la busqueda",
               duration: Toast.duration.SHORT
            });
      })
    }

    guardarFavorito():void {
        const db = require("my-db");
        console.log("Está abierta? ", db.isOpen() ? "Yes" : "No");
        db.execSQL("insert into favoritos (texto) values (?)", ['textFielValue'], function(err, id) {
         console.log("Item guardado:", id);
        });
    }
    leerAhora(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));

    }
}
