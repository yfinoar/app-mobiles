import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";


@Component({
    selector: "SearchForm",
    moduleId: module.id,
    template: `
    <TextField [(ngModel)] = "textFieldValue" hint="Ingresar texto .."></TextField>
    <Button text="Button" (tap)="onButtonTap()"></Button>
    `,
    // providers: [NoticiasService]
})
export class SearchFormComponent implements OnInit{
    ngOnInit(): void {
       this.textFieldValue = this.inicial;
    }
    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();
    @Input() inicial: string;
    onButtonTap(): void{
        console.log(this.textFieldValue);
        if(this.textFieldValue.length > 2){
            this.search.emit(this.textFieldValue);
        }
    }
}