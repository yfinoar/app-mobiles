import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { EdicionRoutingModule } from "./edicion-routing.module";
import { EdicionComponent } from "./edicion.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        EdicionRoutingModule
    ],
    declarations: [
        EdicionComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class EdicionModule { }
