import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";

@Component({
    selector: "Edicion",
    template: `
    <TextField [(ngModel)] = "nombreUsuario" hint="Ingresar texto .."></TextField>
    <Button text="Button" (tap)="onButtonTap()"></Button>
    `})
export class BrowseComponent implements OnInit {
    
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        let LS = require( "nativescript-localstorage" );
        localStorage.setItem('nombreUsuario', 'Ximena');
        LS.getItem('nombreUsuario');
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
