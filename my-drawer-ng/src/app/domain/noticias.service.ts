import {Injectable} from "@angular/core";
import {getJSON, request} from "tns-core-modules/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasService {
    api:String =  "https://888497e1817c.ngrok.io";
    constructor(){
        this.getDb((db) => {
            db.each("Select * from logs",
            (err, fila)=> console.log("Fila:", fila),
            (err, totales) => console.log("Totales:", totales));
        }, () => console.log("Error on getDb"));
    }
    getDb(fnOk, fnError){
        return new sqlite("mi_db_logs", (err, db)=>{
            if(err){
                console.log("Error al abrir db!", err);
            } else {
                console.log("Está la db abierta:", db.isOpen() ? "Si": "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                .then((id)=> {
                    console.log("CREATE TABLE OK");
                    fnOk(db);
                }, (error) =>{
                    console.log("CREATE TABLE ERROR", error);
                    fnError(error);
                });
            }
        })

    }
    
    agregar(s: string){
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-Type": "application/json"},
            content:JSON.stringify({
                nuevo: s
            })
        });
    }
    favs(){
        return getJSON(this.api + "/favs");
    }
    buscar(s: string){
        this.getDb((db)=> {
            db.execSQL("insert into logs (texto) values (?)", [s],
            (err, id)=> console.log("nuevo id", id));
        }, () => console.log("error on getDb"));
        
        return getJSON(this.api + "/get?q=" + s);
    }
}