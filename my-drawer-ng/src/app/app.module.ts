import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "@nativescript/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NoticiasService } from "./domain/noticias.service";
import { FuncionalidadModule } from "./funcionalidad/funcionalidad.module";
import { initializeNoticiasState,
NoticiasState,
NoticiasEffects,
reducersNoticias } from "./domain/noticias-state.model";
import { EffectsModule } from "@ngrx/effects";

export interface AppState{
    noticias: NoticiasState;
}
const reducers: ActionReducerMap<AppState> = {
    noticias: reducersNoticias
};
const reducersInitialState = {
    noticias: initializeNoticiasState()
};
@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState}),
        EffectsModule.forRoot([NoticiasEffects])
    ],
    declarations: [
        AppComponent,
        FuncionalidadModule
    ],
    providers: [NoticiasService],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
