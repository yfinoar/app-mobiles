import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Dialogs } from "@nativescript/core";
import * as dialogs from 'tns-core-modules/ui/dialogs';
import * as Toast from "nativescript-toast";
import * as appSettings from "tns-core-modules/application-settings"; 


@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }
    doLater(fn) {setTimeout(fn, 1000);}

    ngOnInit(): void {
    //   this.doLater(() =>
    //   dialogs.action("Mensaje", "Cancelar", ["Opcion1", "Opcion2"])
    //   .then((result) =>{
    //         console.log("resultado:" +result);
    //         if(result === "Opcion1"){
    //             this.doLater(()=>{
    //                 Dialogs.alert({
    //                     title: "Titulo 1",
    //                     message: "Mensaje 1",
    //                     okButtonText: "Btn 1"
    //                 }).then(()=> console.log("Cerrado 1!"));
    //             })
    //         } else if(result === "Opcion2"){
    //             this.doLater(()=>
    //             Dialogs.alert({
    //                 title: "Titulo 2",
    //                 message: "Mensaje 2",
    //                 okButtonText: "Btn 2"
    //             }).then(()=> console.log("Cerrado2")));
    //         }
    //   }));
    const toastOptions: Toast.ToastOptions = {
        text: "Hello world",
        duration: Toast.DURATION.SHORT};
        this.doLater(() => Toast.show(toastOptions));
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
